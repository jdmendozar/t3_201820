package model.logic;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOStations;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.DoubleLinkedList.elIterator;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class DivvyTripsManager implements IDivvyTripsManager {

	private DoubleLinkedList<VOStations> stations;
	private Queue<VOTrip> tripsQ;
	private Stack<VOTrip> tripsS;
	
	
	public void loadStations (String stationsFile) {
		
		stations = new DoubleLinkedList<VOStations>();
		File file = new File(stationsFile);

		
		try {
			CSVReader reader = new CSVReader (new FileReader(file));
			reader.readNext();
			String[] nextline;
			while((nextline=reader.readNext()) != null)
			{
				if(nextline != null)
				{

					int id = Integer.parseInt(nextline[0]);

					String nombre = nextline[1];

					String nombreCiudad = nextline[2];

					int dpc = Integer.parseInt(nextline[5]);

					VOStations nuevo = new VOStations(id, nombre, nombreCiudad, dpc);

					stations.addAtK(0, nuevo);

					nextline = null;
				}
			}
			reader.close();
		} catch ( IOException e ) 
		{
			e.printStackTrace();
		}
	}
	
	public void loadTrips (String tripsFile) {
		// TODO Auto-generated method stub
		
		tripsQ = new Queue<VOTrip>();
		tripsS = new Stack<VOTrip>();
		
		File file = new File(tripsFile);
		
		try
		{
			CSVReader reader = new CSVReader (new FileReader(file));
			reader.readNext();
			String[] nextline;
			
			while((nextline=reader.readNext()) != null)
			{
				if(nextline != null)
				{
					int id = Integer.parseInt(nextline[0]);

					int bikeID = Integer.parseInt(nextline[3]);
					
					int duratioN = Integer.parseInt(nextline[4]);
					
					int fromId = Integer.parseInt(nextline[5]);
					
					String fromName = nextline[6];
					
					int toId = Integer.parseInt(nextline[7]);
					
					String toName = nextline[8];
					
					String gendeR = null;
					if(nextline.length>10)
					{
					 gendeR = nextline[10];
					}
					VOTrip nuevo = new VOTrip(id, bikeID, duratioN, fromId, fromName, toId, toName, gendeR);

					tripsQ.enqueue(nuevo);
					tripsS.push(nuevo);
					
				}
			}
			reader.close();
			
		}
		catch ( IOException e ) 
		{

			e.printStackTrace();
		}

		
	}
	
	@Override
	public IDoublyLinkedList <String> getLastNStations (int bicycleId, int n) 
	{
		int contador=0;
		
		Iterator<VOTrip> iter = tripsS.iterator();
		
		DoubleLinkedList<String> resp = new DoubleLinkedList<String>();
		
		while(iter.hasNext()== true && contador < n)
		{
			VOTrip actual = iter.next();
			
			if(actual.getBikeId()== bicycleId)
			{
				resp.addObject(actual.getFromStation());
				contador++;
			}
		}
		return resp;
	}

	@Override
	public VOTrip customerNumberN (int stationID, int n) 
	{
		int contador= 1;
		
		VOTrip resp= null ;
		
		Iterator<VOTrip> alterno = tripsS.iterator();
		
		
		boolean termino = false;
		
		while(alterno.hasNext()== true && !termino)
		{

			VOTrip actual = alterno.next();
			
			if(actual.getToStationId()== stationID)
			{
				if(contador==n)
				{
					resp = actual;
					termino = true;
				}
				contador++;
			}
			
		}
			
		return resp;
	}	


}
