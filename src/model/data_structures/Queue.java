package model.data_structures;

import java.util.Iterator;

class Node<T> 
{
	private Node<T> siguiente;
	
	private Node<T> anterior;

	private T object;

	public Node (T ob)
	{
		object= ob;
	}

	public void cambiarSiguiente(Node<T> sis)
	{
		siguiente = sis;
	}
	
	public void cambiarAnterior (Node<T> ant)
	{
		anterior = ant;
	}

	public T darObjeto()
	{
		return object;
	}

	public Node<T> darSiguiente()
	{
		return siguiente;
	}
	
	public Node<T> darAnterior()
	{
		return anterior;
	}


}

public class Queue <T> implements IQueue<T> {

	private Node<T> primero;
	
	private int size;
	
	private Node<T> ultimo;
	
	@SuppressWarnings("hiding")
	public class elIterator<T> implements Iterator<T>
	{

		private Node<T> actual;

		private int cont;

		@SuppressWarnings("unchecked")
		public elIterator()
		{
			actual = (Node<T>) primero;
		}
		@Override
		public boolean hasNext() 
		{
			
			if(actual != null)
			{
				return actual.darSiguiente()!= null || cont ==0;
			}
			return false;
		}

		@Override
		public T next() 
		{
			if(cont == 0)
			{
				cont ++;
			}else
			{
				actual= actual.darSiguiente();
			}
			return actual.darObjeto();
		}

	}

	@Override
	public Iterator<T> iterator() {
		
		return new elIterator<T>();
	}

	@Override
	public boolean isEmpty() {
		if(size!=0)
		return false;
		else 
			return true;
	}

	@Override
	public int size() {
		
		return size;
	}

	@Override
	public void enqueue(T t) {
		Node<T> nuevo = new Node<T>(t);
		
		if(ultimo == null && primero ==null)
		{
			ultimo = nuevo;
			primero = nuevo;
		}else
		{
			ultimo.cambiarAnterior(nuevo);
			nuevo.cambiarSiguiente(ultimo);
			ultimo = nuevo;
		}
		size++;	
	}

	@Override
	public T dequeue() 
	{
		Node<T> anterior = primero.darAnterior();
		
		Node<T> resp = primero;
		
		if(primero == ultimo)
		{
			primero = null;
			
			ultimo = null;
			size=0;
		}else 
		{
			anterior.cambiarSiguiente(null);
			
			primero = anterior;
			size--;
		}
		
		return resp.darObjeto();
	}

}
