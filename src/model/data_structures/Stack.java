/**
 * 
 */
package model.data_structures;

import java.util.Iterator;


/**
 * @author jdmen
 *
 */

public class Stack <T>implements IStack<T> 
{
	private Node<T> primero;
	
	private int size;
	
	public class elIterator<T> implements Iterator<T>
	{

		private Node<T> actual;

		private int cont;

		public elIterator()
		{
			actual = (Node<T>) primero;
		}
		@Override
		public boolean hasNext() 
		{
			
			if(actual != null)
			{
				return actual.darSiguiente()!= null || cont ==0;
			}
			return false;
		}

		@Override
		public T next() 
		{
			if(cont == 0)
			{
				cont ++;
			}else
			{
				actual= actual.darSiguiente();
			}
			return actual.darObjeto();
		}

	}

	@Override
	public Iterator<T> iterator() {
		
		return new elIterator<T>();
	}

	@Override
	public boolean isEmpty() {
		if(size == 0)
			return true;
		else
		return false;
	}

	@Override
	public int size() {
		
		return size;
	}

	@Override
	public void push(T t) {
		
		Node<T> nuevoPrimero = new Node(t);
		if(primero != null)
		{
			
			
			nuevoPrimero.cambiarSiguiente(primero);
			
			primero = nuevoPrimero;
			size++;
		}else
		{
		primero = nuevoPrimero;
		size++;
		}
		
	}

	@Override
	public T pop() 
	{
		Node<T> aPop = primero;
		
		if(primero.darSiguiente() != null)
		{
			primero = primero.darSiguiente();
			
			size--;
			return aPop.darObjeto();
		}else if(primero != null)
		{
			primero = null;
			size--;
			return aPop.darObjeto();
		}else
		{
			return null;
		}		
	}

	@Override
	public T peek() 
	{
		if(primero != null)
		{
			return primero.darObjeto();
		}else
		{
		return null;
		}
	}
	
}
