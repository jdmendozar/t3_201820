/**
 * 
 */
package model.data_structures;

import java.util.Iterator;

/**
 * @author jdmen
 *
 */


public class DoubleLinkedList<T> implements IDoublyLinkedList<T> 
{
	private Node<T> primero;

	private int size;

	@SuppressWarnings("hiding")
	public class elIterator<T> implements Iterator<T>
	{

		private Node<T> actual;

		private int cont;

		public elIterator()
		{
			actual = (Node<T>) primero;
		}
		@Override
		public boolean hasNext() 
		{
			
			if(actual != null)
			{
				return actual.darSiguiente()!= null || cont ==0;
			}
			return false;
		}

		@Override
		public T next() 
		{
			if(cont == 0)
			{
				cont ++;
			}else
			{
				actual= actual.darSiguiente();
			}
			return actual.darObjeto();
		}

	}

	public Node<T> searchObject(T t)
	{
		Node<T> buscado = null;

		Node<T> nodoActual = primero;

		while(nodoActual != null && buscado == null)
		{
			if(nodoActual.darObjeto().equals(t))
			{
				buscado = nodoActual;
			}else
			{
				nodoActual = nodoActual.darSiguiente();
			}
		}

		return buscado;
	}

	@Override
	public Iterator<T> iterator() {

		return new elIterator<T>();
	}

	@Override
	public Integer getSize() {

		return size;
	}

	@Override
	public T getFirst() 
	{
		return primero.darObjeto();
	}

	@Override
	public void addObject(T t) 
	{
		Node<T> actual = primero;
		if(primero == null)
		{
			primero = new Node<T>(t);
			size++;
			return;
		}
		else
		{
			while(actual.darSiguiente() != null)
			{
				actual = actual.darSiguiente();
			}
			Node<T> a = new Node<T>(t);
			a.cambiarAnterior(actual);
			actual.cambiarSiguiente(a);
			size++;
		}

	}


	@Override
	public void addAtK(int pos, T e) 
	{
		Node<T> actual = primero;
		Node<T> agregar = new Node<T>(e);
		if(pos == 0)
		{
			primero = new Node<T>(e);
			primero.cambiarSiguiente(actual);
			size++;
		}else if(pos == size)
		{
			addObject(e);
			return;
		}else if (pos < size)
		{
			int i =1;
			while (i <= pos)
			{
				actual = actual.darSiguiente();
				i++;
			}
			Node<T> anterior = actual.darAnterior();
			agregar.cambiarAnterior(anterior);
			agregar.cambiarSiguiente(actual);
			anterior.cambiarSiguiente(agregar);
			actual.cambiarAnterior(agregar);
			size++;
		}
	}

	@Override
	public T getElement(int pos) 
	{
		Node<T> nodo = primero;
		
		for (int i = 1; i <= pos; i++) 
		{
			nodo = nodo.darSiguiente();
		}
		return nodo.darObjeto();
	}

	@Override
	public void remove(T t) throws Exception 
	{
		Node<T> aElimi = primero;
		if(primero.darObjeto().equals(aElimi))
		{
			primero = aElimi.darSiguiente();
			aElimi.cambiarAnterior(null);
			size--;
			
		}else
		{
			boolean b = false;
			
			while(aElimi.darSiguiente() !=null && !b)
			{
				aElimi = aElimi.darSiguiente();
				b= aElimi.darObjeto().equals(aElimi)?true:false;
			}
			Node<T> ant= aElimi.darAnterior();
			Node<T> sig = aElimi.darSiguiente();
			ant.cambiarSiguiente(sig);
			if(sig != null)
			{
				sig.cambiarAnterior(ant);
			}
			size--;
		}
	}

	@Override
	public void removeAtK(int pos) throws Exception 
	{
		Node<T> actual = primero;
		if(pos >= size && pos <0)
		{
			throw new Exception("Posicion fuera de lista.");
		}
		if(pos == 0)
		{
			primero = primero.darSiguiente();
			primero.cambiarAnterior(null);
			size--;
		}else
		{
			for (int i = 1; i < pos; i++) {
				actual = actual.darSiguiente();
			}
			Node<T> anterior = actual.darAnterior();
			if(actual.darSiguiente()!=null)
			{
				actual.darSiguiente().cambiarAnterior(anterior);
			}
			anterior.cambiarSiguiente(actual.darSiguiente());
			size--;
		}
	}
}
