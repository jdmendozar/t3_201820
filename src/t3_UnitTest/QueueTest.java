package t3_UnitTest;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.Queue;

/**
 * 
 * @author jdmen
 *
 */
public class QueueTest {

	private Queue<Integer> fila;
	@Test
	public void test() {
		
		fila = new Queue<Integer>();
		assertTrue("La fila debe empezar vacia.", fila.size()==0);

		fila.enqueue(5);
		
		assertTrue("La fila debe tener un dato", fila.size()==1);

		Integer cont1 = fila.dequeue();
		assertTrue("No se retorno el n�mero esperado: 5",cont1==5);
		assertTrue("El tama�o no se redujo.", fila.size()==0);

		fila.enqueue(1);
		fila.enqueue(2);
		fila.enqueue(3);
		fila.enqueue(4);
		fila.enqueue(5);
		assertTrue("El tama�o deber�a ser: 5", fila.size()==5);

		Integer[] arreglo1 = new Integer[5];

		for (int i = 0; i < 5; i++) 
		{
			arreglo1[i] = fila.dequeue();
		}

		assertTrue("La fila deber�a estar vac�a:"+ fila.size(),fila.size()==0);

		for (int i = 0; i < 5; i++) 
		{
			assertTrue("El elemento no corresponde a lo agregado.",arreglo1[i]== i+1);
		}
	}

}
