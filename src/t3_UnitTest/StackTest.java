/**
 * 
 */
package t3_UnitTest;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.Stack;

/**
 * @author jdmen
 *
 */
public class StackTest {

	private Stack<Integer> pila = new Stack<Integer>();
	@Test
	public void test() {
		
		assertTrue("La pila deber�a estar vacia.",pila.size()==0);
		
		pila.push(5);
		assertTrue("El elemento 5 no se encuentra en la pila.",pila.peek()==5);
		assertTrue("La pila no aument� su tama�o.",pila.size()==1);
		
		Integer resp1= pila.pop();
		
		assertTrue("No se hizo correctamente la operaci�n pop.",resp1==5);
		assertTrue("El tama�o de la pila no decreci�.", pila.size()==0);
		
		
		pila.push(5);
		pila.push(4);
		pila.push(3);
		pila.push(2);
		pila.push(1);
		
		assertTrue("La pila deber�a ser de tama�o 5 y es"+pila.size(), pila.size()==5);
		
		Integer[] arreg1 = new Integer[5];
		
		for (int i = 0; i < arreg1.length; i++) 
		{
			arreg1[i]= pila.pop();
		}
		
		assertTrue(pila.size()==0);
		
		for (int i = 0; i < arreg1.length; i++) 
		{
			assertTrue("La operaci�n pop no di� el resultado esperado."+(i+1),arreg1[i]==i+1);
		}
		
	}

}
